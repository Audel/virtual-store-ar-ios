//
//  Product.swift
//  VirtualSuperDemo
//
//  Created by Audel Dugarte on 2/13/18.
//  Copyright © 2018 Audel Dugarte. All rights reserved.
//

import Foundation

class ProductOfStore{
    
    var productName : String?
    var productId : String?
    var productUnitPrice : Double = 0.0
    var productKilogramPrice : Double = 0.0
    var quantity = 0
    
}
