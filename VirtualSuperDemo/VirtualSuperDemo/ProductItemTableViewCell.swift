//
//  ProductItemTableViewCell.swift
//  VirtualSuperDemo
//
//  Created by Audel Dugarte on 2/13/18.
//  Copyright © 2018 Audel Dugarte. All rights reserved.
//

import UIKit

class ProductItemTableViewCell: UITableViewCell {

    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productUnitPrice: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    
    var flag = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onStarFavButtonTapped(_ sender: UIButton) {
        flag = !flag
        let imgName = flag ? "star_full" : "star_empty"
        let image1 = UIImage(named: imgName)!
        sender.setImage(image1, for: .normal)
    }
    
}
