//
//  ModalCartViewController.swift
//  VirtualSuperDemo
//
//  Created by Audel Dugarte on 4/4/18.
//  Copyright © 2018 Audel Dugarte. All rights reserved.
//

import UIKit

class ModalCartViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, DecreaseOrRemoveCellButtonsDelegate {

    @IBOutlet weak var productsInCartTableView: UITableView!
    
    
    private var data: [String]?
    
    public var shoppingCartData: [ProductOfStore]?
    
    var grandTotal = 0.0
    
    //function as variable to enable communication with the view controller that called this one
    var onCloseWithDataChanged : ( (_ shoppingCartData: [ProductOfStore]) -> () )?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        productsInCartTableView.dataSource = self
        productsInCartTableView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: ' Buttons Actions
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        onCloseWithDataChanged?(self.shoppingCartData!)
        dismiss(animated: false) {
        }
    }
    
    //MARK: - Sendingg data to checkout view controller
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? CheckotViewController {
            destinationViewController.shoppingCartData = shoppingCartData
         }
    }
    
    //NARK: - Conforming to UITableViewDataSource protocol to add data to the tableview
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return data.count
        return shoppingCartData!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productInCartCellReusableIdentifier") as! ProductInCartItemTableViewCell //1.
        
        let text = "\(shoppingCartData![indexPath.row].productName!) x \(shoppingCartData![indexPath.row].quantity)"
        let productPrice = shoppingCartData![indexPath.row].productUnitPrice
        let price = "unit: \(productPrice)"
        let finalPrice = productPrice * Double(shoppingCartData![indexPath.row].quantity)
        
        //NSLog("texto que deberia ir es \(String(describing: shoppingCartData[indexPath.row].productName))")
        
        cell.productInCartName?.text = text //3.
        cell.productIncartUnitPrice?.text = "\(price)"
        cell.productInCartPrice?.text = "\(finalPrice)"
        
        cell.indexPath = indexPath
        cell.delegate = self
        
        return cell //4.
    }

    //MARK: - conforming to the UITAbleView Delegate to enable interaction with the table
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NSLog(" Selected item number  \(indexPath.row)")
    }
    
    //MARK: - Conforming to delegate of tableviewcell that has a bitton to remove items from current cart
    func decreaseOrRemoveTapped(at index:IndexPath){
        NSLog("taped item at \(index.row)")
        let currentQUantity = self.shoppingCartData![index.row].quantity
        if(currentQUantity > 1){
            self.shoppingCartData![index.row].quantity = currentQUantity - 1
        }else{
            self.shoppingCartData!.remove(at: index.row)
        }
        
        self.productsInCartTableView!.reloadData()
    }

}
