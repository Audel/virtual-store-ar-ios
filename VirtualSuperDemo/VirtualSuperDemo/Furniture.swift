//
//  Furniture.swift
//  VirtualSuperDemo
//
//  Created by Audel Dugarte on 10/23/17.
//  Copyright © 2017 Audel Dugarte. All rights reserved.
//

import ARKit

class Furniture: SCNNode {

    func loadModel(modelPath:String){
    
        guard let virtualObjectScene = SCNScene(named: modelPath) else{
            NSLog("could not load model!")
            return
            
        }
        
        let wrapperNode = SCNNode()
        
        for child in virtualObjectScene.rootNode.childNodes{
            wrapperNode.addChildNode(child)
        }
        
        self.addChildNode(wrapperNode)
    }
}
