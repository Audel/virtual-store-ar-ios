//
//  CheckotViewController.swift
//  VirtualSuperDemo
//
//  Created by Audel Dugarte on 2/12/18.
//  Copyright © 2018 Audel Dugarte. All rights reserved.
//

import UIKit

class CheckotViewController: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var productsTableView: UITableView!
    @IBOutlet weak var cheackouTitleAmount: UILabel!
    
    private var data: [String]?
    
    public var shoppingCartData: [ProductOfStore]?

    var grandTotal = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        /*for i in 0...1000 {
            data!.append("\(i)")
        }
        
        for h in 0...10{
            let pr = ProductOfStore()
            pr.productName = "producto\(h)"
            pr.productId = "0000\(h)"
            pr.productUnitPrice = 1000.0 + Double(h)
            pr.quantity = Int( randomPosition(loweBound: 1.0, upperBound: 10.0))
            shoppingCartData!.append(pr)
            
            grandTotal = grandTotal + pr.productUnitPrice * Double(pr.quantity)
        }*/
        
        for pr in shoppingCartData!{
            grandTotal = grandTotal + pr.productUnitPrice * Double(pr.quantity)
        }
        
        productsTableView.dataSource = self
        
        cheackouTitleAmount.text = "Checkout your items (total: \(grandTotal)$)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //NARK: - Conforming to UITableViewDataSource protocol
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return data.count
        return shoppingCartData!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productCellReusableIdentifier") as! ProductItemTableViewCell //1.
        
        let text = "\(shoppingCartData![indexPath.row].productName!) x \(shoppingCartData![indexPath.row].quantity)"
        let productPrice = shoppingCartData![indexPath.row].productUnitPrice
        let price = "unit: \(productPrice)"
        let finalPrice = productPrice * Double(shoppingCartData![indexPath.row].quantity)
        
        //NSLog("texto que deberia ir es \(String(describing: shoppingCartData[indexPath.row].productName))")
        
        cell.productName?.text = text //3.
        cell.productUnitPrice?.text = "\(price)"
        cell.productPrice?.text = "\(finalPrice)"
        
        return cell //4.
    }

    //MARK:- other functions
    
    func randomPosition(loweBound lower: Float, upperBound upper:Float)->Float{
        return Float(arc4random())/Float(UInt32.max) * (lower-upper) + upper
    }

    @IBAction func onBackButtonPressed(_ sender: UIButton) {
        dismiss(animated: false, completion: nil)
    }
    

    //MARK: - sending the info to the checkout activity
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //send back the shopping cart list in case user needs to review something in it
        if let destinationViewController = segue.destination as? ViewController {
            NSLog("called prepare for segue and recognized as going back to ViewController")
            destinationViewController.cartListData = shoppingCartData!
        }
    }
}
