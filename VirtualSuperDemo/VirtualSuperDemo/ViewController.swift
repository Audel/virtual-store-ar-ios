//
//  ViewController.swift
//  VirtualSuperDemo
//
//  Created by Audel Dugarte on 10/23/17.
//  Copyright © 2017 Audel Dugarte. All rights reserved.
//

import UIKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var countreLabel: UILabel!
    
    let shelfModel = Furniture()
    let shelfModelLeft = Furniture()
    
    var screenCenter : CGPoint?
    
    var shelfModelRe: SCNNode?
    var shelfModelLoaded = false
    var shelfModelLoadedinLeft = false
    
    var amountInCart:Double = 0.0 {
        didSet{
            self.countreLabel.text = "Total: $ \(amountInCart)"
        }
    }
    
    var cartListData : [ProductOfStore]?
    var actualProductsOnShelf : [ProductOfStore]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        sceneView.delegate = self
        
        let scene = SCNScene()
        sceneView.scene = scene
        
        screenCenter = view.center
        
        cartListData = []
        actualProductsOnShelf = creteDummyStore()
        
        //add gesture recognizer to handle user inputs
        /*var gestureRecognizerSwipeUp = UISwipeGestureRecognizer(target: self, action: #selector(activatedGestureRecognizer))
        gestureRecognizerSwipeUp.direction = UISwipeGestureRecognizerDirection.up
        self.sceneView.addGestureRecognizer(gestureRecognizerSwipeUp)
        
        var gestureRecognizerSwipeDown = UISwipeGestureRecognizer(target: self, action: #selector(activatedGestureRecognizerDown))
        gestureRecognizerSwipeDown.direction = .down
        self.sceneView.addGestureRecognizer(gestureRecognizerSwipeDown)
        
        let swipeRightOrange:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(slideToRightWithGestureRecognizer))
        swipeRightOrange.direction = .right;
        self.sceneView.addGestureRecognizer(swipeRightOrange)*/
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        /*if ARWorldTrackingConfiguration.isSupported {
            let configuration = ARWorldTrackingConfiguration()
            self.sceneView.session.run(configuration)
        } else if ARConfiguration.isSupported {
            let configuration = ARConfiguration.
            self.sceneView.session.run(configuration)
        }*/
        
        self.sceneView.debugOptions  = [.showConstraints, .showLightExtents, ARSCNDebugOptions.showFeaturePoints, ARSCNDebugOptions.showWorldOrigin]
        //shows fps rate
        
        //old simplest way
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = ARWorldTrackingConfiguration.PlaneDetection.horizontal
        sceneView.session.run(configuration)
        
        //addObject()
        let tempScene = SCNScene(named: "Models.scnassets/dae/test_model.DAE")!
        shelfModelRe = tempScene.rootNode.childNode(withName: "Group02", recursively: true)!
        //addShelfModel()
        //addProductsImages()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.sceneView.session.pause()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addObject(){
        let candleFurn = Furniture()
        //candleFurn.loadModel(modelPath: "Models.scnassets/candle/candle.scn")
        candleFurn.loadModel(modelPath: "Models.scnassets/dae/test_model.DAE")
        
        /*let chairFurn = Furniture()
        chairFurn.loadModel(modelPath: "Models.scnassets/chair/chair.scn")
        chairFurn.position = SCNVector3(randomPosition(loweBound: -1.5, upperBound: 1.5), randomPosition(loweBound: -1.5, upperBound: 1.5), -1)
        sceneView.scene.rootNode.addChildNode(chairFurn)*/
        
        let xPos = randomPosition(loweBound: -10.5, upperBound: 10.5)
        let yPos = randomPosition(loweBound: -10.5, upperBound: 10.5)
        candleFurn.position = SCNVector3(xPos, yPos, -15)
        candleFurn.scale = SCNVector3(0.2, 0.2, 0.2)
        
        sceneView.scene.rootNode.addChildNode(candleFurn)
    }
    
    func addShelfModel(){
        //let shelfModel = Furniture()
        
        shelfModel.loadModel(modelPath: "Models.scnassets/dae/test_model.DAE")
        
        shelfModel.position = SCNVector3(50, -45, -60)
        //shelfModel.scale = SCNVector3(0.2, 0.2, 0.2)
        
        sceneView.scene.rootNode.addChildNode(shelfModel)
    }
    
    func addProductsImages(){
        var products: [SCNNode] = []
        
        products.append( make2dNode(image: UIImage(named:"images_products/cereal.jpg")!, width: 0.5, height: 0.5) )
        products.append( make2dNode(image: UIImage(named:"images_products/flour.jpeg")!, width: 0.5, height: 0.5) )
        products.append( make2dNode(image: UIImage(named:"images_products/noodles.jpg")!, width: 0.5, height: 0.5) )
        //second row
        products.append( make2dNode(image: UIImage(named:"images_products/beans.jpeg")!, width: 0.5, height: 0.5) )
        products.append( make2dNode(image: UIImage(named:"images_products/tomatosauce.jpg")!, width: 0.5, height: 0.5) )
        products.append( make2dNode(image: UIImage(named:"images_products/canned_corn.jpeg")!, width: 0.5, height: 0.5) )
        //third row
        products.append( make2dNode(image: UIImage(named:"images_products/cannedtuna.jpg")!, width: 0.5, height: 0.5) )
        products.append( make2dNode(image: UIImage(named:"images_products/olive_oil.jpg")!, width: 0.5, height: 0.5) )
        products.append( make2dNode(image: UIImage(named:"images_products/box_milk.jpg")!, width: 0.5, height: 0.5) )
        var productDisplacement = 0.0
        
        var contP = 0
        
        for aNode in products {
            //aNode.position = SCNVector3(40, -5, -67-productDisplacement)
            if contP <= 2{
                aNode.position = SCNVector3(Float(1.231708), Float(0.097), Float(-1.34 - productDisplacement))
            }else if contP <= 5 && contP > 2{
                if(contP==3){productDisplacement = 0.0}
                aNode.position = SCNVector3(Float(1.291708), Float(-1.097), Float(-1.34 - productDisplacement))
            }else if contP <= 8 && contP > 5 {
                if(contP==6){productDisplacement = 0.0}
                aNode.position = SCNVector3(Float(1.231708), Float(-1.997), Float(-1.03 - productDisplacement))
            }
            //aNode.name = "image\(productDisplacement)"
            aNode.name = actualProductsOnShelf![contP].productId
            contP+=1
            sceneView.scene.rootNode.addChildNode(aNode)
            //productDisplacement += 20
            productDisplacement = productDisplacement + 0.4
        }
        
        //other part
        /*var thingoNode = make2dNode(image: UIImage(named:"images_products/tuna.png")! , width: 5, height: 5)
        let xPos = randomPosition(loweBound: -10.5, upperBound: 10.5)
        let yPos = randomPosition(loweBound: -10.5, upperBound: 10.5)
        thingoNode.name = "tunahola"
        thingoNode.position = SCNVector3(xPos, yPos, -15)
        sceneView.scene.rootNode.addChildNode(thingoNode)*/
    }
    
    func addProductsLeftImages(){
        var products: [SCNNode] = []
        
        products.append( make2dNode(image: UIImage(named:"images_products/cereal.jpg")!, width: 0.5, height: 0.5) )
        products.append( make2dNode(image: UIImage(named:"images_products/flour.jpeg")!, width: 0.5, height: 0.5) )
        products.append( make2dNode(image: UIImage(named:"images_products/noodles.jpg")!, width: 0.5, height: 0.5) )
        //second row
        products.append( make2dNode(image: UIImage(named:"images_products/beans.jpeg")!, width: 0.5, height: 0.5) )
        products.append( make2dNode(image: UIImage(named:"images_products/tomatosauce.jpg")!, width: 0.5, height: 0.5) )
        products.append( make2dNode(image: UIImage(named:"images_products/canned_corn.jpeg")!, width: 0.5, height: 0.5) )
        //third row
        products.append( make2dNode(image: UIImage(named:"images_products/cannedtuna.jpg")!, width: 0.5, height: 0.5) )
        products.append( make2dNode(image: UIImage(named:"images_products/olive_oil.jpg")!, width: 0.5, height: 0.5) )
        products.append( make2dNode(image: UIImage(named:"images_products/box_milk.jpg")!, width: 0.5, height: 0.5) )
        var productDisplacement = 0.0
        
        var contP = 0
        
        for aNode in products {
            //aNode.position = SCNVector3(40, -5, -67-productDisplacement)
            if contP <= 2{
                aNode.position = SCNVector3(Float(-1.731708), Float(0.097), Float(-1.34 - productDisplacement))
            }else if contP <= 5 && contP > 2{
                if(contP==3){productDisplacement = 0.0}
                aNode.position = SCNVector3(Float(-1.791708), Float(-1.097), Float(-1.34 - productDisplacement))
            }else if contP <= 8 && contP > 5 {
                if(contP==6){productDisplacement = 0.0}
                aNode.position = SCNVector3(Float(-1.731708), Float(-1.997), Float(-1.03 - productDisplacement))
            }
            //aNode.name = "image\(productDisplacement)"
            aNode.name = actualProductsOnShelf![contP].productId
            contP+=1
            sceneView.scene.rootNode.addChildNode(aNode)
            //productDisplacement += 20
            productDisplacement = productDisplacement + 0.4
        }
        
        //other part
        /*var thingoNode = make2dNode(image: UIImage(named:"images_products/tuna.png")! , width: 5, height: 5)
         let xPos = randomPosition(loweBound: -10.5, upperBound: 10.5)
         let yPos = randomPosition(loweBound: -10.5, upperBound: 10.5)
         thingoNode.name = "tunahola"
         thingoNode.position = SCNVector3(xPos, yPos, -15)
         sceneView.scene.rootNode.addChildNode(thingoNode)*/
    }
    
    func make2dNode(image: UIImage, width: CGFloat = 0.7, height: CGFloat = 0.7) -> SCNNode {
        let plane = SCNPlane(width: width, height: height)
        plane.firstMaterial!.diffuse.contents = image
        let node = SCNNode(geometry: plane)
        node.constraints = [SCNBillboardConstraint()]
        return node
    }
    
    func randomPosition(loweBound lower: Float, upperBound upper:Float)->Float{
        return Float(arc4random())/Float(UInt32.max) * (lower-upper) + upper
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        NSLog("touched the screen")
        
        if let touch = touches.first{
            
            NSLog("touch found in touches list")
            
            let location = touch.location(in: sceneView)
            
            let hitList = sceneView.hitTest(location, options: nil)
            
            //addshelfNode(touch: touch)
            
            if let hitObject = hitList.first{
                let node = hitObject.node
                
                if let furnTouched = node as? Furniture{
                    NSLog("node can be also determined by its furniture atributes")
                }
                
                if let nodeName = node.name as? String {
                    NSLog( "node touched is: \(nodeName)")
                    
                    switch(nodeName){
                    case "image0" :
                        NSLog("cereal selected")
                        showAlertDialog(title: "articulo seleccionado", message: "has seleccionado cereal")
                    case "image20" :
                        NSLog("flour selected")
                        showAlertDialog(title: "articulo seleccionado", message: "has seleccionado harina")
                    case "image40":
                        NSLog("noodles seceted")
                        showAlertDialog(title: "articulo seleccionado", message: "has seleccionado fideos")
                    default:
                        NSLog("testing with a new loop")
                        for mPrd in actualProductsOnShelf!{
                            NSLog("comparing \(mPrd.productId!) with \(nodeName)")
                            if(mPrd.productId! == nodeName){
                                /*showAlertDialog(title: "articulo** seleccionado", message: mPrd.productName!)
                                addProductToCart(prod: mPrd)*/
                                showAddProductAlertDialog(title: "Agregar articulo seleccionado", message: mPrd.productName!, productToAdd: mPrd)
                            }
                        }
                    }
                }else{
                    return
                }
                
                //old code from game tutorial used to guide us
                /*if(node.name!.contains("candle") ){
                    hitsCounter += 1
                    node.removeFromParentNode()
                    addObject()
                }else if(node.name!.contains("chair") ){
                    hitsCounter += 1
                    node.removeFromParentNode()
                    addObject()
                }*/
            }
        }
    }
    
    func showAlertDialog(title : String, message : String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let defaultAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { (pAlert) in
            //Do whatever you wants here
            NSLog("Pressed OK button")
        })
        alert.addAction(defaultAction)
        alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAddProductAlertDialog(title : String, message : String, productToAdd : ProductOfStore){
        let fullProductDescription = "\(message) ($xKg: \(productToAdd.productKilogramPrice))"
        let alert = UIAlertController(title: title, message: fullProductDescription, preferredStyle: UIAlertControllerStyle.alert)
        let defaultAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { (pAlert) in
            //Do whatever you wants here
            NSLog("Pressed OK button")
            self.addProductToCart(prod: productToAdd)
            self.updatetotalInCartLaber()
        })
        alert.addAction(defaultAction)
        alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func addProductToCart(prod : ProductOfStore){
        for mprd in cartListData! {
            if(mprd.productId! == prod.productId!){
                mprd.quantity+=1
                return
            }
        }
        prod.quantity=1
        cartListData?.append(prod)
    }
    
    func updatetotalInCartLaber(){
        //also increase amount and use computed value to update label
        amountInCart = 0.0
        for pr in cartListData!{
            amountInCart = amountInCart + pr.productUnitPrice * Double(pr.quantity)
        }
    }
    
    //- Mark: gesture recognizer actions
    
    @IBAction func activatedGestureRecognizer(gesture: UIGestureRecognizer) {
        if let gestureRecognizer = gesture as? UIGestureRecognizer {
            
            // Here you can compare using if gestureRecognizer == gestureRecognizerSwipeRight
            // ...or you could compare the direction of the gesture recognizer.
            // It all depends on your implementation really.
            
            NSLog("swiped up deteced")
            NSLog("swiped down deteced")
            for sceneChild in sceneView.scene.rootNode.childNodes{
                sceneChild.position = SCNVector3( sceneChild.position.x, sceneChild.position.y, sceneChild.position.z+10)
            }
            
            /*if gestureRecognizer == gestureRecognizerSwipeUp {
                // Swipe right detected
                NSLog("swiped up deteced")
            }*/
        }
    }
    
    @IBAction func activatedGestureRecognizerDown(gesture: UIGestureRecognizer) {
        if let gestureRecognizer = gesture as? UIGestureRecognizer {
        
            /*
            let translation = gestureRecognizer.translation(in: self.sceneView)
            //let result : SCNVector3 = CGPointToSCNVector3(view: self.sceneView, depth: currentTouchLocationInPlane.z, point: translation)
            let result : SCNVector3 = SCNVector3Make(Float(translation.x), Float(tappedObjectNode.position.y),currentTouchLocationInPlane.z)
            tappedObjectNode.position = result
            */
            
            NSLog("swiped down deteced")
            for sceneChild in sceneView.scene.rootNode.childNodes{
                sceneChild.position = SCNVector3( sceneChild.position.x, sceneChild.position.y, sceneChild.position.z-10)
            }
        }
    }
    
    @IBAction func slideToRightWithGestureRecognizer(gestureRecognizer:UISwipeGestureRecognizer)
    {
        //viewOrange.backgroundColor = UIColor.lightGray
        NSLog("swiped right")
    }
    
    /*2017-11-10 09:40:12.493531-0300 VirtualSuperDemo[4130:2534373] node touched is: Optional("chair_seat")
     2017-11-10 09:40:36.961143-0300 VirtualSuperDemo[4130:2534373] touched the screen
     2017-11-10 09:40:36.961306-0300 VirtualSuperDemo[4130:2534373] touch found in touches list
     2017-11-10 09:40:36.961892-0300 VirtualSuperDemo[4130:2534373] node touched is: Optional("shadowPlane")
     2017-11-10 09:40:49.244980-0300 VirtualSuperDemo[4130:2534373] touched the screen
     2017-11-10 09:40:49.245283-0300 VirtualSuperDemo[4130:2534373] touch found in touches list
     2017-11-10 09:40:49.245964-0300 VirtualSuperDemo[4130:2534373] node touched is: Optional("candle_wax")
     2017-11-10 09:40:55.328692-0300 VirtualSuperDemo[4130:2534373] touched the screen
     2017-11-10 09:40:55.329232-0300 VirtualSuperDemo[4130:2534373] touch found in touches list
     2017-11-10 09:40:57.128779-0300 VirtualSuperDemo[4130:2534373] touched the screen
     2017-11-10 09:40:57.128908-0300 VirtualSuperDemo[4130:2534373] touch found in touches list
     2017-11-10 09:41:01.679230-0300 VirtualSuperDemo[4130:2534373] touched the screen
     2017-11-10 09:41:01.679382-0300 VirtualSuperDemo[4130:2534373] touch found in touches list
     2017-11-10 09:41:03.196370-0300 VirtualSuperDemo[4130:2534373] touched the screen
     2017-11-10 09:41:03.196633-0300 VirtualSuperDemo[4130:2534373] touch found in touches list
     2017-11-10 09:41:03.196820-0300 VirtualSuperDemo[4130:2534373] node touched is: Optional("candle_wax")
     2017-11-10 09:41:06.596183-0300 VirtualSuperDemo[4130:2534373] touched the screen
     2017-11-10 09:41:06.596341-0300 VirtualSuperDemo[4130:2534373] touch found in touches list
     2017-11-10 09:41:06.596571-0300 VirtualSuperDemo[4130:2534373] node touched is: Optional("candle_holder")
     2017-11-10 09:41:10.446134-0300 VirtualSuperDemo[4130:2534373] touched the screen
     2017-11-10 09:41:10.446297-0300 VirtualSuperDemo[4130:2534373] touch found in touches list
     2017-11-10 09:41:10.446480-0300 VirtualSuperDemo[4130:2534373] node touched is: Optional("candle_wax")
     2017-11-10 09:41:11.428665-0300 VirtualSuperDemo[4130:2534373] touched the screen
     2017-11-10 09:41:11.428901-0300 VirtualSuperDemo[4130:2534373] touch found in touches list
     2017-11-10 09:41:13.962931-0300 VirtualSuperDemo[4130:2534373] touched the screen
     2017-11-10 09:41:13.963084-0300 VirtualSuperDemo[4130:2534373] touch found in touches list
     2017-11-10 09:41:16.146208-0300 VirtualSuperDemo[4130:2534373] touched the screen
     2017-11-10 09:41:16.146353-0300 VirtualSuperDemo[4130:2534373] touch found in touches list
     2017-11-10 09:41:18.362922-0300 VirtualSuperDemo[4130:2534373] touched the screen
     2017-11-10 09:41:18.363042-0300 VirtualSuperDemo[4130:2534373] touch found in touches list
     2017-11-10 09:41:18.363411-0300 VirtualSuperDemo[4130:2534373] node touched is: Optional("candle_wax")*/
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        // This visualization covers only detected planes.
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        
        /*
        // Create a SceneKit plane to visualize the node using its position and extent.
        let plane = SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(planeAnchor.extent.z))
        let planeNode = SCNNode(geometry: plane)
        planeNode.position = SCNVector3Make(planeAnchor.center.x, 0, planeAnchor.center.z)
        */
        
        //updateShelfWithAnchor(anchor: planeAnchor)
        //shelfModelRe!.position = SCNVector3Make(planeAnchor.center.x, 0, planeAnchor.center.z)
        //sceneView.scene.rootNode.addChildNode(shelfModelRe!)
        if(!shelfModelLoaded){
            addShelfOnPlane()
        }
        if(!shelfModelLoadedinLeft){
            addLeftShelfOnPlane()
        }
        
        /*
        // SCNPlanes are vertically oriented in their local coordinate space.
        // Rotate it to match the horizontal orientation of the ARPlaneAnchor.
        planeNode.transform = SCNMatrix4MakeRotation(-Float.pi / 2, 1, 0, 0)
        
        // ARKit owns the node corresponding to the anchor, so make the plane a child node.
        node.addChildNode(planeNode)
        //node.addChildNode(shelfModelRe!)
        */
    }
    
    func updateShelfWithAnchor(anchor : ARPlaneAnchor){
        //50, -45, -60
        NSLog("recalculating sheld position with anchor \(anchor.center.x), \(anchor.center.y), \(anchor.center.z)")
        shelfModel.position = SCNVector3Make(anchor.center.x + 50, -45, anchor.center.z - 60)
        shelfModel.transform = SCNMatrix4MakeRotation(-Float.pi / 2, 1, 0, 0)
    }
    
    func addshelfNode(touch: UITouch){
        
        let results = sceneView.hitTest(touch.location(in: sceneView), types: [ARHitTestResult.ResultType.featurePoint])
        guard let hitFeature = results.last else { return }
        let hitTransform = SCNMatrix4.init(hitFeature.worldTransform) // <- if higher than beta 1, use just this -> hitFeature.worldTransform
        let hitPosition = SCNVector3Make(hitTransform.m41,
                                         hitTransform.m42,
                                         hitTransform.m43)
        
        let hitPositionAgain = SCNVector3Make(hitFeature.worldTransform.columns.3.x, hitFeature.worldTransform.columns.3.y, hitFeature.worldTransform.columns.3.z)
        
        
        shelfModel.loadModel(modelPath: "Models.scnassets/dae/test_model.DAE")
        
        shelfModel.position = hitPositionAgain
        //shelfModel.position = SCNVector3(50, -45, -60)
        shelfModel.scale = SCNVector3(0.02, 0.02, 0.02)
        
        sceneView.scene.rootNode.addChildNode(shelfModel)
    }
    
    func addShelfOnPlane() {
        //let planeHitTestResults = sceneView.hitTest(view.center, types: .existingPlaneUsingExtent)
        let planeHitTestResults = sceneView.hitTest(screenCenter!, types: .existingPlaneUsingExtent)
        if let result = planeHitTestResults.first {
            
            /*
            let hitTransform = SCNMatrix4.init(result.worldTransform)
            
            NSLog("setting shelf with coordinates \(result.worldTransform.columns.3.x), \(result.worldTransform.columns.3.y), \(result.worldTransform.columns.3.z)")
            let hitPosition = SCNVector3Make(result.worldTransform.columns.3.x, result.worldTransform.columns.3.y, result.worldTransform.columns.3.z)
            */
            
            let hitTransform = SCNMatrix4.init(result.worldTransform) // <- if higher than beta 1, use just this -> hitFeature.worldTransform
            NSLog("setting shelf with coordinates \(hitTransform.m41), \(hitTransform.m42), \(hitTransform.m43)")
            /*let hitPosition = SCNVector3Make(hitTransform.m41,
                                             hitTransform.m42,
                                             hitTransform.m43)*/
            let hitPosition = SCNVector3Make(1.539635, -2.7, -1.0)
            
            let sphere = SCNSphere(radius: 0.005)
            sphere.firstMaterial?.diffuse.contents = UIColor.red
            sphere.firstMaterial?.lightingModel = .constant
            sphere.firstMaterial?.isDoubleSided = true
            let node = SCNNode(geometry: sphere)
            node.position = hitPosition
            sceneView.scene.rootNode.addChildNode(node)
            
            //working one
            //shelfModel.loadModel(modelPath: "Models.scnassets/dae/test_model.DAE")
            
            //shelfModel.loadModel(modelPath: "Models.scnassets/dae/bsr_shelf.dae")
            
            //testing new shelf model
            //shelfModel.loadModel(modelPath: "Models.scnassets/dae/marmol/estante seupermercado 14-03-2018.dae")
            shelfModel.loadModel(modelPath: "Models.scnassets/dae/marmol/estante_supermercado-23-04-18-OK-2.obj (3).dae")
            
            shelfModel.position = hitPosition
            //shelfModel.position = SCNVector3(50, -45, -60)
            //scale for benjamin model
            //shelfModel.scale = SCNVector3(0.2, 0.2, 0.2)
            shelfModel.scale = SCNVector3(0.06, 0.06, 0.06)
            
            sceneView.scene.rootNode.addChildNode(shelfModel)
            addProductsImages()
            shelfModelLoaded = true
        }
    }
    
    func addLeftShelfOnPlane() {
        //let planeHitTestResults = sceneView.hitTest(view.center, types: .existingPlaneUsingExtent)
        let planeHitTestResults = sceneView.hitTest(screenCenter!, types: .existingPlaneUsingExtent)
        if let result = planeHitTestResults.first {
            
            let hitTransform = SCNMatrix4.init(result.worldTransform) // <- if higher than beta 1, use just this -> hitFeature.worldTransform
            NSLog("setting left shelf with coordinates \(hitTransform.m41), \(hitTransform.m42), \(hitTransform.m43)")
            
            let hitPosition = SCNVector3Make(-3.539635, -2.7, -1.0)
            
            let sphere = SCNSphere(radius: 0.005)
            sphere.firstMaterial?.diffuse.contents = UIColor.red
            sphere.firstMaterial?.lightingModel = .constant
            sphere.firstMaterial?.isDoubleSided = true
            let node = SCNNode(geometry: sphere)
            node.position = hitPosition
            sceneView.scene.rootNode.addChildNode(node)
            
            //working one
            shelfModelLeft.loadModel(modelPath: "Models.scnassets/dae/marmol/estante_supermercado-23-04-18-OK-2.obj (left).dae")
            
            shelfModelLeft.position = hitPosition
            //shelfModelLeft.rotation = SCNVector4Make(0.0, 180.5, 0.0, 0.0)
            //scale for benjamin model
            shelfModelLeft.scale = SCNVector3(0.06, 0.06, 0.06)
            
            sceneView.scene.rootNode.addChildNode(shelfModelLeft)
            addProductsLeftImages()
            shelfModelLoadedinLeft = true
        }
    }
    
    //MARK: - sending the info to the checkout activity
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        /*for h in 0...10{
            let pr = ProductOfStore()
            pr.productName = "producto\(h)"
            pr.productId = "0000\(h)"
            pr.productUnitPrice = 1000.0 + Double(h)
            pr.quantity = Int( randomPosition(loweBound: 1.0, upperBound: 10.0))
            cartListData!.append(pr)
        }*/
        
        if let destinationViewController = segue.destination as? ModalCartViewController {
            destinationViewController.shoppingCartData = cartListData
            //callback functions used to communicate back
            //use closure for interface-like function to receive data from modalviewcontroller called from the segue
            destinationViewController.onCloseWithDataChanged = { (shoppingCartData: [ProductOfStore]) -> () in
                self.cartListData = shoppingCartData
                self.updatetotalInCartLaber()
            }
        }
    }

    //MARK:- create dummy store items
    func creteDummyStore()->[ProductOfStore]{
        var mDummy : [ProductOfStore] = []
        
        var anyProduvt = ProductOfStore()
        anyProduvt.productId = "0000100"
        anyProduvt.productName = "cereal (300g)"
        anyProduvt.productUnitPrice = 1200.0
        anyProduvt.productKilogramPrice = 4000.0
        mDummy.append(anyProduvt)
        anyProduvt = ProductOfStore()
        anyProduvt.productId = "0000203"
        anyProduvt.productName = "Harina (500g)"
        anyProduvt.productUnitPrice = 1950.0
        anyProduvt.productKilogramPrice = 3900.0
        mDummy.append(anyProduvt)
        anyProduvt = ProductOfStore()
        anyProduvt.productId = "0003300"
        anyProduvt.productName = "fideos (400g)"
        anyProduvt.productUnitPrice = 750.0
        anyProduvt.productKilogramPrice = 1875.0
        mDummy.append(anyProduvt)
        //second row
        anyProduvt.productId = "0002100"
        anyProduvt.productName = "Porotos (300g)"
        anyProduvt.productUnitPrice = 1200.0
        anyProduvt.productKilogramPrice = 4000.0
        mDummy.append(anyProduvt)
        anyProduvt = ProductOfStore()
        anyProduvt.productId = "0002203"
        anyProduvt.productName = "Salsa de tomate (500g)"
        anyProduvt.productUnitPrice = 1950.0
        anyProduvt.productKilogramPrice = 3900.0
        mDummy.append(anyProduvt)
        anyProduvt = ProductOfStore()
        anyProduvt.productId = "0002320"
        anyProduvt.productName = "Choclos (400g)"
        anyProduvt.productUnitPrice = 750.0
        anyProduvt.productKilogramPrice = 1875.0
        mDummy.append(anyProduvt)
        //thiird row
        anyProduvt.productId = "0003100"
        anyProduvt.productName = "Atún en lata (300g)"
        anyProduvt.productUnitPrice = 1200.0
        anyProduvt.productKilogramPrice = 4000.0
        mDummy.append(anyProduvt)
        anyProduvt = ProductOfStore()
        anyProduvt.productId = "0003203"
        anyProduvt.productName = "Aceite de oliva (500g)"
        anyProduvt.productUnitPrice = 1950.0
        anyProduvt.productKilogramPrice = 3900.0
        mDummy.append(anyProduvt)
        anyProduvt = ProductOfStore()
        anyProduvt.productId = "0003420"
        anyProduvt.productName = "Leche (1000 ml)"
        anyProduvt.productUnitPrice = 750.0
        anyProduvt.productKilogramPrice = 1875.0
        mDummy.append(anyProduvt)
        
        return mDummy
    }
}

