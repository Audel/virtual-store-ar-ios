//
//  ClothesTesterViewController.swift
//  VirtualSuperDemo
//
//  Created by Audel Dugarte on 6/26/18.
//  Copyright © 2018 Audel Dugarte. All rights reserved.
//

import UIKit
import ARKit

class ClothesTesterViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet weak var testerSceneView: ARSCNView!
    
    var screenCenter : CGPoint?
    
    var isShirtLoaded = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //yep
        
        testerSceneView.debugOptions  = [.showConstraints, .showLightExtents, ARSCNDebugOptions.showFeaturePoints, ARSCNDebugOptions.showWorldOrigin]
        
        //old simplest way
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = ARWorldTrackingConfiguration.PlaneDetection.horizontal
        testerSceneView.session.run(configuration)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //yep
        testerSceneView.delegate = self
        
        screenCenter = view.center
        
        let scene = SCNScene()
        testerSceneView.scene = scene
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.testerSceneView.session.pause()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func make2dNode(image: UIImage, width: CGFloat = 0.7, height: CGFloat = 0.7) -> SCNNode {
        let plane = SCNPlane(width: width, height: height)
        plane.firstMaterial!.diffuse.contents = image
        let node = SCNNode(geometry: plane)
        node.constraints = [SCNBillboardConstraint()]
        return node
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        // This visualization covers only detected planes.
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        
        /*
         // Create a SceneKit plane to visualize the node using its position and extent.
         let plane = SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(planeAnchor.extent.z))
         let planeNode = SCNNode(geometry: plane)
         planeNode.position = SCNVector3Make(planeAnchor.center.x, 0, planeAnchor.center.z)
         */
        
        if(!isShirtLoaded){
            addShirtOnPlane()
        }
        
    }
    
    func addShirtOnPlane(){
        let planeHitTestResults = testerSceneView.hitTest(screenCenter!, types: .existingPlaneUsingExtent)
        if let result = planeHitTestResults.first {
            
            let hitTransform = SCNMatrix4.init(result.worldTransform) // <- if higher than beta 1, use just this -> hitFeature.worldTransform
            NSLog("setting left shelf with coordinates \(hitTransform.m41), \(hitTransform.m42), \(hitTransform.m43)")
            
            let hitPosition = SCNVector3Make(0.0, 0.0, -1.0)
            
            let sphere = SCNSphere(radius: 0.005)
            sphere.firstMaterial?.diffuse.contents = UIColor.red
            sphere.firstMaterial?.lightingModel = .constant
            sphere.firstMaterial?.isDoubleSided = true
            let node = SCNNode(geometry: sphere)
            node.position = hitPosition
            testerSceneView.scene.rootNode.addChildNode(node)
            
            //working one
            //var shirtNodeObject = make2dNode(image: UIImage(named:"images_products/beans.jpeg")!, width:0.5, height:0.5 )
            var shirtNodeObject = make2dNode(image: UIImage(named:"images_products/green_shirt.png")!, width:0.5, height:0.5 )
            
            shirtNodeObject.position = hitPosition
            //shelfModelLeft.rotation = SCNVector4Make(0.0, 180.5, 0.0, 0.0)
            //scale for benjamin model
            shirtNodeObject.scale = SCNVector3(0.06, 0.06, 0.06)
            
            testerSceneView.scene.rootNode.addChildNode(shirtNodeObject)
            isShirtLoaded = true
        }
    }
}
