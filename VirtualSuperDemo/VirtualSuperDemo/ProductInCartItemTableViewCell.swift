//
//  ProductInCartItemTableViewCell.swift
//  VirtualSuperDemo
//
//  Created by Audel Dugarte on 4/6/18.
//  Copyright © 2018 Audel Dugarte. All rights reserved.
//

import UIKit

protocol DecreaseOrRemoveCellButtonsDelegate{
    func decreaseOrRemoveTapped(at index:IndexPath)
}

class ProductInCartItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var productInCartName: UILabel!
    @IBOutlet weak var productIncartUnitPrice: UILabel!
    @IBOutlet weak var productInCartPrice: UILabel!
    @IBOutlet weak var removeItemButton: UIButton!
    
    var indexPath:IndexPath!
    var delegate : DecreaseOrRemoveCellButtonsDelegate!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //handler for the button tapped
    @IBAction func didTapDecreaseOrRemoveButton(_ sender: UIButton) {
        self.delegate.decreaseOrRemoveTapped(at: self.indexPath)
    }
    

}
